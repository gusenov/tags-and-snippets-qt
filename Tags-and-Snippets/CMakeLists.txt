cmake_minimum_required(VERSION 3.5)

project(Tags-and-Snippets LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check http://doc.qt.io/qt-5/deployment-android.html for more information.
# They need to be set before the find_package(Qt5 ...) call.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

find_package(Qt5 COMPONENTS Core Quick LinguistTools Widgets WebEngineWidgets WebChannel REQUIRED)

set(TS_FILES Tags-and-Snippets_ru_RU.ts)

set(SOURCES
    main.cpp
    DisplayFileSystemModel.cpp
    Document.cpp
    PreviewPage.cpp
    ${TS_FILES}
)

# Resources:
set(resource_files
    "resources/qml.qrc"
)
qt5_add_resources(SOURCES ${resource_files})

if(ANDROID)
    add_library(Tags-and-Snippets SHARED
      ${SOURCES}
    )
else()
    add_executable(Tags-and-Snippets
      ${SOURCES}
    )
endif()

target_compile_definitions(Tags-and-Snippets
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(Tags-and-Snippets
  PRIVATE Qt5::Core Qt5::Quick Qt5::Qml Qt5::Widgets Qt5::WebEngineWidgets Qt5::WebChannel)

qt5_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})

include(${CMAKE_CURRENT_SOURCE_DIR}/qmllive.cmake)
add_qmllive(${PROJECT_NAME})
