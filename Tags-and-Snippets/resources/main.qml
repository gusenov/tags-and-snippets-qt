import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.5
import QtQml.Models 2.2
import QtWebEngine 1.1
import QtWebChannel 1.0
import io.qt.examples.quick.controls.filesystembrowser 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Tags & Snippets")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    function readFile(url)
    {
        var xhr = new XMLHttpRequest
        xhr.open("GET", url)
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                var response = xhr.responseText
                documentText.setText(response)
            }
        };
        xhr.send();
    }

    SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "lightblue"

            SplitView {
                anchors.fill: parent
                orientation: Qt.Horizontal

                Rectangle {
                    width: 200
                    color: "lightblue"

                    ItemSelectionModel {
                        id: sel
                        model: fileSystemModel
                    }

                    TreeView {
                        id: view
                        anchors.fill: parent
                        model: fileSystemModel
                        rootIndex: rootPathIndex
                        selection: sel
                        onActivated : {
                        }
                        selectionMode: SelectionMode.SingleSelection

                        TableViewColumn {
                            title: "Name"
                            role: "fileName"
                            resizable: true
                        }
                    }

                }

                Rectangle {
                    Layout.fillWidth: true
                    color: "lightgray"

                    TreeView {
                        id: tableView
                        anchors.fill: parent
                        model: fileSystemModel
                        rootIndex: sel.currentIndex

                        onActivated: (index) => {
                                         console.log(Object.keys(index))
                                         sel.setCurrentIndex(index, ItemSelectionModel.ClearAndSelect)
                                     }

                        onClicked: (index) => {
                                       var url = fileSystemModel.data(index, FileSystemModel.UrlStringRole)
                                       readFile(url)
                                   }

                        TableViewColumn {
                            title: "Name"
                            role: "fileName"
                            resizable: true
                        }

                        TableViewColumn {
                            title: "Size"
                            role: "size"
                            resizable: true
                            horizontalAlignment : Text.AlignRight
                            width: 70
                        }

                        TableViewColumn {
                            title: "Permissions"
                            role: "displayableFilePermissions"
                            resizable: true
                            width: 100
                        }

                        TableViewColumn {
                            title: "Date Modified"
                            role: "lastModified"
                            resizable: true
                        }
                    }
                }
            }
        }

        Rectangle {
            height: 200
            Layout.fillWidth: true
            color: "lightgreen"

            WebEngineView {
                anchors.fill: parent
                url: "qrc:/index.html"
                webChannel: channel
            }
        }

    }
}
