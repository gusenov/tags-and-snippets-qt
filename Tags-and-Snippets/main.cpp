#include <QApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include <QQmlWebChannel>

#include "DisplayFileSystemModel.h"
#include "PreviewPage.h"
#include "Document.h"

#ifdef QT_DEBUG
#include "qmllive.h"
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/images/favicon.svg"));

    QQmlApplicationEngine engine;

    qmlRegisterUncreatableType<DisplayFileSystemModel>("io.qt.examples.quick.controls.filesystembrowser", 1, 0,
                                                       "FileSystemModel", "Cannot create a FileSystemModel instance.");
    QFileSystemModel *fsm = new DisplayFileSystemModel(&engine);
    qDebug() << QDir::homePath();
    fsm->setRootPath(QDir::homePath());
    fsm->setResolveSymlinks(true);
    engine.rootContext()->setContextProperty("fileSystemModel", fsm);
    engine.rootContext()->setContextProperty("rootPathIndex", fsm->index(fsm->rootPath()));

    Document content;
    engine.rootContext()->setContextProperty("documentText", &content);

    PreviewPage *page = new PreviewPage(&engine);
    QQmlWebChannel *channel = new QQmlWebChannel(&engine);
    channel->registerObject(QStringLiteral("content"), &content);
    page->setWebChannel(channel);
    engine.rootContext()->setContextProperty("channel", channel);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

#ifdef QT_DEBUG
    LiveNodeEngineRunner::GetInstance().Run(engine, url);
#endif

    return app.exec();
}
